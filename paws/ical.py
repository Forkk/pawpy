import re
from datetime import datetime
from icalendar import Calendar, Event

time_regex = r'(\d\d):(\d\d)([AP]M)'
def class_datetime(date, time):
    dparts = list(map(int, date.split('/')))
    tmatch = re.match(time_regex, time)
    hr = int(tmatch.group(1))
    mn = int(tmatch.group(2))
    if tmatch.group(3) == "PM":
        if hr == 12: hr = 0
        else: hr += 12
    return datetime(dparts[2], dparts[0], dparts[1], hr, mn, 0)

def to_ical_dow(d):
    """Converts a TigerPAWS style MTWRF day of week string into an ical style
    MO TU WE TH FR day of week"""
    if d == 'M': return 'MO'
    if d == 'T': return 'TU'
    if d == 'W': return 'WE'
    if d == 'R': return 'TH'
    if d == 'F': return 'FR'

def sched_to_cal(sched):
    c = Calendar()
    for cls in sched:
        evt = class_to_event(cls)
        if evt:
            c.add_component(evt)
    return c

def class_to_event(cls):
    if cls.meet.start_time == 'N/A': return None
    # The times the class starts and ends on the first day.
    dtstart = class_datetime(cls.meet.start_date, cls.meet.start_time)
    dtend = class_datetime(cls.meet.start_date, cls.meet.end_time)
    clsend = class_datetime(cls.meet.end_date, cls.meet.end_time)

    rrdays = list(map(to_ical_dow, cls.meet.days))

    e = Event()
    e.add('summary', cls.title)
    e.add('dtstart', dtstart)
    e.add('dtend', dtend)
    e.add('location', cls.meet.room)
    e.add('rrule', {'freq': 'weekly', 'byday': rrdays, 'until': clsend})
    return e
