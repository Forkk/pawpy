#!/usr/bin/env python3

# This script implements a PyQt5-based GUI for TigerPAWS using the pawpy Python
# library.

import os.path
import sys
from PyQt5.QtCore import pyqtSlot, pyqtSignal, QThread
from PyQt5.QtWidgets import *

import paws.sess
from paws.ical import sched_to_cal

class MainWin(QMainWindow):
    def __init__(self, sess, terms):
        super().__init__()
        self.sess = sess
        self.setWindowTitle("TigerPAWS")
        self.resize(800, 600)

        self.tabs = QTabWidget()
        self.lazyTabs = []

        self.searchBox = SearchBox(self.sess, self, parent=self.tabs, terms=terms)
        self.tabs.addTab(self.searchBox, "Search for Sections")

        self.schedBox = ScheduleBox(self.sess, terms, self.tabs)
        self.tabs.addTab(self.schedBox, "Class Schedule")
        self.lazyTabs.append(self.schedBox)

        self.prefSectsBox = PrefSectsBox(self.sess, self.tabs)
        self.tabs.addTab(self.prefSectsBox, "Preferred Sections")
        self.lazyTabs.append(self.prefSectsBox)

        self.regStatusBox = RegStatusBox(self.sess, self.tabs)
        self.tabs.addTab(self.regStatusBox, "Registration Status")
        self.lazyTabs.append(self.regStatusBox)

        self.setCentralWidget(self.tabs)
        self.tabs.currentChanged.connect(self.current_changed)
        self.show()

    @pyqtSlot()
    def pref_sects_changed(self):
        """
        This is called from the search results dialog whenever the user adds
        classes to preferred sections. If the preferred sections box is
        currently displayed, this will reload it. Otherwise it will be reloaded
        next time it's displayed.
        """
        if self.tabs.currentWidget() == self.prefSectsBox:
            self.prefSectsBox.load()
        else:
            self.prefSectsBox.set_loaded(False)
            self.tabs.setCurrentWidget(self.prefSectsBox)

    @pyqtSlot()
    def current_changed(self):
        item = self.tabs.currentWidget()
        if item in self.lazyTabs:
            item.load_once()


nameCols = [
    ("Course ID", lambda c: c.cid),
    ("Synonym", lambda c: c.shortid),
    ("Title", lambda c: c.title),
]

meetInfoCols = [
    ("Days", lambda c: "".join(c.meet.days)),
    ("Start Time", lambda c: c.meet.start_time),
    ("End Time", lambda c: c.meet.end_time),
    ("Room", lambda c: c.meet.room),
]

defCols = nameCols + meetInfoCols

class ClassListView(QTreeWidget):
    def __init__(self, cols=defCols, multi=False):
        super().__init__()
        self.cols = cols
        self.setHeaderLabels([l for (l, _) in self.cols])
        if multi: self.setSelectionMode(QTreeWidget.MultiSelection)

    def getSelected(self):
        """Returns a list of selected classes."""
        classes = []
        for s in self.selectedItems():
            classes.append(self.classes[s.data(len(self.cols), 0)])
        return classes

    def showClasses(self, classes):
        """Clears the list view and displays the given list of classes."""
        self.clear()
        self.classes = classes
        for i, c in enumerate(classes):
            item = QTreeWidgetItem(self)
            item.setData(len(self.cols), 0, i)
            for (i, (l, func)) in enumerate(self.cols):
                item.setText(i, func(c))
            self.addTopLevelItem(item)
        for i, _ in enumerate(self.cols):
            self.resizeColumnToContents(i)


class SearchResultsWin(QWidget):
    def __init__(self, results, mainWin):
        super(SearchResultsWin, self).__init__()
        self.mainWin = mainWin
        self.layout = QVBoxLayout(self)

        self.setWindowTitle('Search Results')
        self.resize(1000, 300)

        self.listView = ClassListView(cols=nameCols + [
            ("Status", lambda c: c.status),
            ("Professor", lambda c: c.prof),
        ] + meetInfoCols, multi=True)
        self.listView.showClasses(results)
        self.layout.addWidget(self.listView)

        self.btnBox = QDialogButtonBox(self)

        self.addBtn = QPushButton("Add Sections")
        self.addBtn.clicked.connect(self.add_pref_sect)
        self.btnBox.addButton(self.addBtn, QDialogButtonBox.ActionRole)

        self.closeBtn = self.btnBox.addButton(QDialogButtonBox.Close)
        self.closeBtn.clicked.connect(self.close)

        self.layout.addWidget(self.btnBox)

    @pyqtSlot()
    def add_pref_sect(self):
        add = []
        for c in self.listView.getSelected():
            add.append(c.shortid)
        if len(add) == 0: return
        try:
            sects = withProgDlg(
                lambda: self.mainWin.sess.add_pref_sects(add),
                label="Adding preferred sections...",
                parent=self,
            )
        except CancelProgDlg:
            return
        self.mainWin.pref_sects_changed()


class LazyLoadMixin(object):
    """
    This is a mixin for panels like the ScheduleBox and RegStatusBox which have
    data that is loaded from TigerPAWS only when the user clicks them.
    """
    def  __init__(self):
        self.loaded = False

    def is_loaded(self):
        return self.loaded

    def set_loaded(self, loaded=True):
        self.loaded = loaded

    def load(self):
        pass

    def load_once(self):
        """
        Calls self.load if is_loaded is false, and then sets it to true.
        """
        if not self.is_loaded():
            self.load()
            self.set_loaded()

class ClassListBox(QWidget, LazyLoadMixin):
    """
    Base class for panels that display a list of classes and some action
    buttons.
    """
    def __init__(self, parent=None, cols=defCols, multi=False):
        super(ClassListBox, self).__init__(parent)
        self.parent = parent

        self.layout = QVBoxLayout(self)
        self.setLayout(self.layout)

        self.listView = ClassListView(cols=cols, multi=multi)
        self.layout.addWidget(self.listView)

        self.btnBox = QDialogButtonBox(self)

        self.layout.addWidget(self.btnBox)

    def add_btn(self, text, role):
        btn = QPushButton(text)
        self.btnBox.addButton(btn, role)
        return btn


class SearchBox(QWidget):
    def __init__(self, sess, mainWin, parent=None, terms=[]):
        super(SearchBox, self).__init__(parent)
        self.sess = sess
        self.parent = parent
        self.mainWin = mainWin
        self.layout = QVBoxLayout(self)

        self.termBox = QComboBox(self)
        for (t, name) in terms:
            self.termBox.addItem(name, t)
        self.layout.addWidget(self.termBox)

        subjBox = QWidget()
        subjLayout = QHBoxLayout()
        subjLayout.setContentsMargins(0, 0, 0, 0)
        subjBox.setLayout(subjLayout)
        self.subjEdit = QLineEdit()
        self.subjEdit.setPlaceholderText("Subject (e.g., CSCI)")
        self.numEdit = QLineEdit()
        self.numEdit.setPlaceholderText("Course Number (e.g., 1320)")
        self.sectEdit = QLineEdit()
        self.sectEdit.setPlaceholderText("Section Number")
        subjLayout.addWidget(self.subjEdit)
        subjLayout.addWidget(self.numEdit)
        subjLayout.addWidget(self.sectEdit)
        self.layout.addWidget(subjBox)

        self.titleEdit = QLineEdit()
        self.titleEdit.setPlaceholderText("Course Title")
        self.layout.addWidget(self.titleEdit)

        dowBox = QWidget()
        dowLayout = QHBoxLayout()
        dowLayout.setContentsMargins(0, 0, 0, 0)
        dowBox.setLayout(dowLayout)
        self.dayChecks = {}
        for day in ['M', 'T', 'W', 'R', 'F']:
            cbox = QCheckBox(to_full_dow(day))
            dowLayout.addWidget(cbox)
            self.dayChecks[day] = cbox

        dowLayout.addStretch()
        self.layout.addWidget(dowBox)

        self.layout.addStretch()

        self.btnBox = QDialogButtonBox(self)

        self.searchBtn = QPushButton("Search")
        self.searchBtn.clicked.connect(self.search_clicked)
        self.btnBox.addButton(self.searchBtn, QDialogButtonBox.AcceptRole)

        self.subjEdit.returnPressed.connect(self.searchBtn.click)
        self.numEdit.returnPressed.connect(self.searchBtn.click)
        self.sectEdit.returnPressed.connect(self.searchBtn.click)
        self.titleEdit.returnPressed.connect(self.searchBtn.click)

        self.layout.addWidget(self.btnBox)

        self.setLayout(self.layout)

        # This list exists purely to keep the garbage collector from deleting
        # our search results windows.
        self.searchWindows = []

    @pyqtSlot()
    def search_clicked(self):
        term = self.termBox.currentData()
        subj = self.subjEdit.text()
        num = self.numEdit.text()
        title = self.titleEdit.text()
        days = filter(lambda d: self.dayChecks[d].isChecked(), ['M', 'T', 'W', 'R', 'F'])
        try:
            sects = withProgDlg(
                lambda: self.sess.search_sections(term=term, subj=subj, num=num, title=title, days=days),
                label="Searching...",
                parent=self,
            )
        except paws.sess.BadQueryErr as e:
            errMsgBox(e.args[0], 'Bad Query', parent=self)
            return
        except CancelProgDlg:
            return
        win = SearchResultsWin(sects, self.mainWin)
        self.searchWindows.append(win)
        win.show()

class ScheduleBox(ClassListBox):
    def __init__(self, sess, terms, parent=None):
        super(ScheduleBox, self).__init__(parent)
        self.sess = sess

        self.termSel = QComboBox(self)
        for (t, name) in terms:
            self.termSel.addItem(name, t)
        self.termSel.currentIndexChanged.connect(self.load)
        self.layout.insertWidget(0, self.termSel)

        self.exportBtn = self.add_btn("Export", QDialogButtonBox.ActionRole)
        self.exportBtn.clicked.connect(self.export)

        self.refreshBtn = self.add_btn("Refresh", QDialogButtonBox.ActionRole)
        self.refreshBtn.clicked.connect(self.load)

    @pyqtSlot()
    def export(self):
        dlg = QFileDialog(self, "Export schedule")
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setAcceptMode(QFileDialog.AcceptSave)
        dlg.setMimeTypeFilters(['text/calendar'])
        if dlg.exec_() == QDialog.Rejected:
            return
        path=dlg.selectedFiles()[0]
        with open(path, 'w') as f:
            f.write(sched_to_cal(self.sched).to_ical().decode('utf-8'))
        print("Wrote to {}".format(path))

    @pyqtSlot()
    def load(self):
        sched = None
        term = self.termSel.currentData()
        try:
            sched = withProgDlg(
                lambda: self.sess.get_schedule(term),
                label="Loading schedule...",
                parent=self
            )
            self.sched = sched
        except CancelProgDlg:
            return
        self.listView.showClasses(sched)
        self.set_loaded()

class PrefSectsBox(ClassListBox):
    def __init__(self, sess, parent=None):
        super(PrefSectsBox, self).__init__(parent, multi=True)
        self.sess = sess

        self.regBtn = self.add_btn("Register", QDialogButtonBox.ActionRole)
        self.regBtn.clicked.connect(self.register)

        self.waitBtn = self.add_btn("Waitlist", QDialogButtonBox.ActionRole)
        self.waitBtn.clicked.connect(self.waitlist)

        self.rmBtn = self.add_btn("Remove", QDialogButtonBox.ActionRole)
        self.rmBtn.clicked.connect(self.remove)

        self.refreshBtn = self.add_btn("Refresh", QDialogButtonBox.ActionRole)
        self.refreshBtn.clicked.connect(self.load)

    def get_selected_shortids(self):
        ids = []
        for c in self.listView.getSelected():
            ids.append(c.shortid)
        return ids

    def act_selection(self, act, label):
        """
        Runs a progress dialog that calls sess.act_pref_sects with the
        currently selected sections and the given action.
        """
        ids = self.get_selected_shortids()
        if len(ids) == 0: return
        try:
            sects = withProgDlg(
                lambda: self.sess.act_pref_sects(ids, act),
                label=label,
                parent=self,
            )
        except paws.sess.BadQueryErr as e:
            errMsgBox(e.args[0], 'Bad Query', parent=self)
            return
        except CancelProgDlg:
            return
        self.load()

    @pyqtSlot()
    def waitlist(self):
        self.act_selection('WL', label='Waitlisting sections...')

    @pyqtSlot()
    def register(self):
        self.act_selection('RG', label='Registering for sections...')

    @pyqtSlot()
    def remove(self):
        self.act_selection('RM', label='Removing preferred sections...')

    @pyqtSlot()
    def load(self):
        sects = None
        try:
            sects = withProgDlg(
                lambda: self.sess.get_pref_sects(),
                label="Checking preferred sections...",
                parent=self,
            )
        except CancelProgDlg:
            return
        self.listView.showClasses(sects)
        self.set_loaded()

class RegStatusBox(QWidget, LazyLoadMixin):
    def __init__(self, sess, parent=None):
        super(RegStatusBox, self).__init__(parent)
        self.sess = sess
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.statusLbl = QLabel(parent=self)
        self.layout.addWidget(self.statusLbl)

        self.layout.addStretch()

        self.btnBox = QDialogButtonBox(self)

        self.refreshBtn = QPushButton('Refresh')
        self.refreshBtn.clicked.connect(self.load)
        self.btnBox.addButton(self.refreshBtn, QDialogButtonBox.ActionRole)

        self.layout.addWidget(self.btnBox)

    @pyqtSlot()
    def load(self):
        lines = None
        try:
            lines = withProgDlg(
                lambda: self.sess.get_reg_status(),
                label="Checking registration status...",
                parent=self,
            )
        except CancelProgDlg:
            return
        self.statusLbl.setText("\n".join(lines))
        self.set_loaded()


class LoginDlg(QDialog):
    def __init__(self, parent=None, err=None):
        super(LoginDlg, self).__init__(parent)

        self.resize(300, 140)

        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        if err is not None:
            self.resize(300, 160)
            self.errLbl = QLabel(err, self)
            self.layout.addWidget(self.errLbl)

        self.userField = QLineEdit()
        self.userField.setPlaceholderText("Username")
        self.layout.addWidget(self.userField)

        self.passField = QLineEdit()
        self.passField.setPlaceholderText("Password")
        self.passField.setEchoMode(QLineEdit.Password)
        self.layout.addWidget(self.passField)

        self.saveCheck = QCheckBox("Save credentials (in plain text at ~/.pawauth)", self)
        self.layout.addWidget(self.saveCheck)

        self.layout.addStretch()

        self.btnBox = QDialogButtonBox()
        self.layout.addWidget(self.btnBox)

        self.loginBtn = QPushButton("Login")
        self.btnBox.addButton(self.loginBtn, QDialogButtonBox.AcceptRole)
        self.loginBtn.clicked.connect(self.accept)

        self.cancelBtn = self.btnBox.addButton(QDialogButtonBox.Cancel)
        self.cancelBtn.clicked.connect(self.reject)

    def get_username(self):
        return self.userField.text()

    def get_passwd(self):
        return self.passField.text()

    def get_save_creds(self):
        return self.saveCheck.isChecked()


def errMsgBox(msg, title="", parent=None):
    msg = QMessageBox(
        QMessageBox.Critical,
        title, msg,
        QMessageBox.Ok,
        parent=parent,
    )
    return msg.exec_()


class BackgroundTask(QThread):
    done = pyqtSignal()

    def __init__(self, func, parent=None):
        super(BackgroundTask, self).__init__(parent)
        self.func = func
        self.result = None
        self.error = None

    def run(self):
        try:
            self.result = self.func()
        except Exception as e:
            self.error = e
            self.result = None
        self.done.emit()

def withProgDlg(func, label="Please wait...", parent=None):
    pdlg = QProgressDialog(parent=parent)
    pdlg.setRange(0, 0)
    pdlg.setLabelText(label)
    thr = BackgroundTask(func)
    thr.done.connect(lambda: pdlg.reset())
    thr.start()
    pdlg.exec_()
    # Wait for the thread to actually finish
    thr.wait()
    if pdlg.wasCanceled():
        raise CancelProgDlg()
    elif thr.error is not None:
        raise thr.error
    else:
        return thr.result

class CancelProgDlg(Exception):
    pass


def to_full_dow(dow):
    if dow == 'M': return 'Monday'
    if dow == 'T': return 'Tuesday'
    if dow == 'W': return 'Wednesday'
    if dow == 'R': return 'Thursday'
    if dow == 'F': return 'Friday'


def sess_func(user, passwd):
    def login():
        sess = paws.sess.log_in(user, passwd)
        return dict(
            sess=sess,
            terms=sess.get_terms(),
        )
    return login

def get_sess():
    """
    Acquires a TigerPAWS session by logging in using either the credentials
    specified in the given args, or a file at ~/.pawauth

    The first line of the file is the username, the second is the password.
    """
    params = None
    path = os.path.expanduser('~/.pawauth')
    if os.path.isfile(path):
        with open(path, 'r') as f:
            lines = f.read().splitlines()
            if len(lines) < 2:
                return gui_login(err='Malformed .pawauth file')
            user = lines[0].strip()
            passwd = lines[1].strip()

            try:
                params = withProgDlg(sess_func(user, passwd), label="Logging in to TigerPAWS...")
            except CancelProgDlg:
                exit()
            except paws.sess.LoginFailedErr as e:
                return gui_login(err='Login from .pawauth failed: {}'.format(e.args[0]))
    else:
        params = gui_login()

    return params

def gui_login(err=None):
    loginDlg = LoginDlg(err=err)
    if not loginDlg.exec_() == QDialog.Accepted:
        exit()
    user = loginDlg.get_username()
    passwd = loginDlg.get_passwd()

    try:
        params = withProgDlg(sess_func(user, passwd), label="Logging in to TigerPAWS...")
    except CancelProgDlg:
        exit()
    except paws.sess.LoginFailedErr as e:
        # Try again
        return gui_login(err="Login failed: {}".format(e.args[0]))

    if loginDlg.get_save_creds():
        save_creds(user, passwd)

    return params

def save_creds(user, passwd):
    path = os.path.expanduser('~/.pawauth')
    with open(path, 'w') as f:
        f.write("{}\n{}\n".format(user, passwd))


if __name__ == "__main__":
    app = QApplication(sys.argv)

    params = get_sess()
    ex = MainWin(**params)
    sys.exit(app.exec_())
