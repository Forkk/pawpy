#!/usr/bin/env python

import os.path
from argparse import ArgumentParser
from getpass import getpass

import paws.sess

def get_sess(args):
    """
    Acquires a TigerPAWS session by logging in using either the credentials
    specified in the given args, or a file at ~/.pawauth

    The first line of the file is the username, the second is the password.
    """
    user = ''
    passwd = ''
    path = os.path.expanduser('~/.pawauth')
    if os.path.isfile(path) and not args.prompt_auth:
        with open(path, 'r') as f:
            lines = f.read().splitlines()
            if len(lines) < 2:
                print("Expected two lines in ~/.pawauth: username and password")
                exit()
            user = lines[0].strip()
            passwd = lines[1].strip()
    else:
        user = input('TigerPAWS username: ')
        passwd = getpass('TigerPAWS password: ')

    return paws.sess.log_in(user, passwd)

def status(args):
    sess = get_sess(args)
    for l in sess.get_reg_status():
        print(l)

def sched(args):
    term = args.term

    sess = get_sess(args)
    sched = sess.get_schedule(term)

    if sched is None:
        print('No such term found')
        return

    if args.ics:
        from paws.ical import sched_to_cal
        print(sched_to_cal(sched).to_ical().decode('utf-8'))
    else:
        for cls in sched:
            print(cls.display())

def search(args):
    sess = get_sess(args)
    sects = sess.search_sections(
        term=args.term, subj=args.subj, num=args.num,
        title=args.title,
    )
    for s in sects:
        print(s.display())

def print_pref_sects(sects):
    for cls in sects:
        print(cls.display())

def ls_pref(args):
    sess = get_sess(args)
    sects = sess.get_pref_sects()
    print_pref_sects(sects)

def add_pref(args):
    sess = get_sess(args)
    sess.add_pref_sects(args.ids)
    sects = sess.get_pref_sects()
    print('Your new preferred sections:')
    print_pref_sects(sects)

def rm_pref(args):
    sess = get_sess(args)
    sess.act_pref_sects(args.ids, 'RM')
    sects = sess.get_pref_sects()
    print('Your new preferred sections:')
    print_pref_sects(sects)

def terms(args):
    sess = get_sess(args)
    trms = sess.get_terms()
    for (tid, name) in trms:
        print('{} ({})'.format(name, tid))

parser = ArgumentParser(description='A command line interface to TigerPAWS')
parser.add_argument('-p', '--prompt-auth', action='store_true', help='force prompt for username and password')
subp = parser.add_subparsers()

pars_status = subp.add_parser('status', help='check your registration status')
pars_status.set_defaults(func=status)

pars_terms = subp.add_parser('terms', help='list all terms')
pars_terms.set_defaults(func=terms)

pars_pref = subp.add_parser('pref', help='list all preferred sections')
subp_pref = pars_pref.add_subparsers()

pars_pref_ls = subp_pref.add_parser('ls', help='list all preferred sections')
pars_pref_ls.set_defaults(func=ls_pref)

pars_pref_add = subp_pref.add_parser('add', help='add courses to preferred sections')
pars_pref_add.add_argument('ids', nargs='+')
pars_pref_add.set_defaults(func=add_pref)

pars_pref_rm = subp_pref.add_parser('rm', help='remove courses from preferred sections')
pars_pref_rm.add_argument('ids', nargs='+')
pars_pref_rm.set_defaults(func=rm_pref)

pars_sched = subp.add_parser('sched', help='list classes in your schedule')
pars_sched.add_argument('-t', '--term', default=None)
pars_sched.add_argument('-i', '--ics', action='store_true',
                        help='prints the schedule in icalendar format, '+
                        'which can be imported into many calendar applications')
pars_sched.set_defaults(func=sched)

pars_search = subp.add_parser('search', help='search for class sections')
pars_search.add_argument('-t', '--term', default=None)
pars_search.add_argument('-s', '--subj', default=None,
                         help='short string for the course\'s subject (e.g., CSCI)')
pars_search.add_argument('-n', '--num', default=None, help='course number')
pars_search.add_argument('-c', '--title', default=None, help='course title keywords')
pars_search.set_defaults(func=search)

args = parser.parse_args()
args.func(args)
