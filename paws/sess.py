# This module implements an interface to Tiger paws.

import time
import dryscrape
from bs4 import BeautifulSoup

import paws.parse as parse


def expect(val, msg=None):
    """If the given argument is None, this function raises a `BadPageErr`.
    Otherwise, it returns its argument."""
    if val is None:
        if msg is None: raise BadPageErr()
        else: raise BadPageErr(msg)
    else:
        return val

def log_in(username, passwd):
    """
    This function takes a username and password, creates a new dryscrape
    session, and attempts to log in to TigerPAWS.

    Raises LoginFailedErr if login fails.
    """
    sess = dryscrape.Session()

    sess.visit("https://tigerpaws.trinity.edu/tplive/TigerPAWS")
    expect(sess.at_xpath("//li[@id='acctLogin']")).left_click()

    expect(sess.at_xpath("//input[@name='USER.NAME']")).set(username)
    expect(sess.at_xpath("//input[@name='CURR.PWD']")).set(passwd)
    expect(sess.at_xpath("//input[@value='Log in']")).click()

    students_btn = sess.at_xpath("//a[@class='WBST_Bars']")
    if students_btn:
        students_btn.left_click()
        return PawSess(sess)
    else:
        err = expect(sess.at_xpath("//div[@class='errorText']")).text()
        raise LoginFailedErr(err)


class PawSess(object):
    """
    Represents a logged in tiger paws session. Provides methods for performing
    various actions.
    """

    def __init__(self, sess):
        self.s = sess
        self.home_url = sess.url()

    def visit_home(self):
        """Sends the internal dryscrape session to the TigerPAWS for students
        homepage."""
        self.s.visit(self.home_url)

    def visit_menu_opt(self, opt):
        """Clicks on the first option in the TigerPAWS for student menu which
        contains the given string. Raises BadPageErr if no such option
        exists."""
        self.visit_home()
        expect(self.s.at_xpath("//span[contains(., '{}')]".format(opt))).parent().click()

    def get_reg_status(self):
        """
        Fetches registration status information and returns the content of the
        textboxes on the status page as a list of three strings, one per box.
        """
        self.visit_menu_opt("Registration Status")
        lines = []
        for i in range(1, 4):
            inp = self.s.at_xpath("//input[@name='LIST.VAR6_{}']".format(i))
            if inp is None: raise BadPageErr("Missing text input field {}".format(i))
            lines.append(inp.value())
        return lines

    def get_terms(self):
        """
        Fetches a list of all the terms in the dropdown on the schedule page
        (the same as on the search page).

        Returns a list containing (id, name) tuples.
        """
        self.visit_menu_opt("My class schedule")
        return parse.parse_term_opts(self.s.body())

    def get_schedule(self, term=None):
        """
        Fetches a description of the class schedule for a given term. The term
        may be either None, or a string in the form '{SP,FL,SU}<YEAR>'. If term
        is None, the schedule at the top of the options list (the current term)
        will be used.

        If the given term name is not in the list of options presented by
        TigerPAWS on the webpage, this function will return None.

        Note that TigerPAWS will not show graded courses in the schedule page,
        so this function is probably only useful for the current term.
        """
        self.visit_menu_opt("My class schedule")
        # Find the term dropdown and select the right term
        term_opt = None
        if term is None:
            self.s.at_xpath("//select[@name='VAR4']")
            # We actually have to get the second option in the list, not the
            # first one, which is blank.
            opts = self.s.xpath('//option')
            term_opt = opts[1] if len(opts) > 1 else None
            #print(term_opt.get_attr('value'))
        else:
            term_opt = self.s.at_xpath("//option[@value='{}']".format(term))
        if term_opt is None:
            return None
        term_opt.select_option()
        self.s.at_xpath("//input[@name='SUBMIT2']").click()
        # Wait for the schedule table
        self.s.at_xpath("//table[@summary='Schedule']")
        return parse.parse_class_table(self.s.body())

    def add_pref_sects(self, ids):
        """
        Given a list of synonyms (or short IDs as I've been calling them in
        this library), this function adds all of their associated courses to
        preferred sections using the express register UI.
        """
        self.visit_menu_opt("Express Registration")
        # There are fields on the page numbered from 1 to 10.
        for i in range(1, 11):
            if i > len(ids): break
            id = ids[i-1]
            field = expect(self.s.at_xpath('//input[@name=\'LIST.VAR1_{}\']'.format(i)))
            field.set(id)
        submit = expect(self.s.at_xpath('//input[@name=\'SUBMIT2\']'))
        submit.click()
        expect(self.s.at_xpath('//table[@summary=\'Preferred\']'))

    def act_pref_sects(self, ids, action):
        """
        Performs various actions on preferred sections. The 'action' param can
        be either "RM" to remove the sections, "WL" to add them to the
        waitlist, or "RG" to register for the sections.
        """
        sects = self.get_pref_sects()
        for i, sect in enumerate(sects):
            if sect.shortid in ids:
                sel = self.s.at_xpath('//select[@name=\'LIST.VAR1_{}\']/option[@value=\'{}\']'.format(i+1, action))
                if sel is None:
                    raise BadQueryErr("No such action: {}".format(action))
                else:
                    sel.select_option()
        expect(self.s.at_xpath('//input[@name=\'SUBMIT2\']')).click()

        errTxt = self.s.at_css('.errorText')
        if self.s.at_xpath('//span[.=\'Registration Results\']'):
            return # Success
        elif errTxt:
            raise BadQueryErr('TigerPAWS says: {}'.format(errTxt.text()))
        else:
            self.s.render('error.png')
            raise BadPageErr('Neither success nor error detected')

    def get_pref_sects(self):
        """
        Gets the list of "preferred sections". This is a list classes get added
        to before the student can register for them.
        """
        self.visit_menu_opt("Add and Drop Classes")
        expect(self.s.at_xpath('//select[@name=\'VAR3\']'))
        return parse.parse_pref_sects(self.s.body())

    def search_sections(self, subj=None, num=None, term=None, title=None, days=[]):
        """
        This function is essentially the "Search for Sections" page. All
        parameters are optional. With the exception of the term parameter, if
        any are left out, their corresponding field will be left blank.
        Sometimes this may result in an error like "Please fill out at least 2
        field(s)".

        As usual, if term is None, the current term is used.
        """
        self.visit_menu_opt("Search for Sections")
        term_opt = None
        if term is None:
            self.s.at_xpath("//select[@name='VAR1']")
            opts = self.s.xpath("//select[@name='VAR1']/option")
            term_opt = opts[1] if len(opts) > 1 else None
        else:
            term_opt = self.s.at_xpath("//select[@name='VAR1']/option[@value='{}']".format(term))
        if term_opt is None:
            raise BadQueryErr('No term "{}" found'.format(term))
        term_opt.select_option()

        if subj is not None:
            opt = self.s.at_xpath("//select[@name='LIST.VAR1_1']/option[@value='{}']".format(subj))
            if opt is None:
                raise BadQueryErr('No subject "{}" found'.format(subj))
            opt.select_option()

        if num is not None:
            self.s.at_xpath("//input[@name='LIST.VAR3_1']").set(num)

        if title is not None:
            self.s.at_xpath("//input[@name='VAR3']").set(title)

        for d in days:
            def check(var):
                lblfor = self.s.at_xpath('//input[@name=\'{}\']'.format(var)).click()
            if d == 'M': check('VAR10')
            if d == 'T': check('VAR11')
            if d == 'W': check('VAR12')
            if d == 'R': check('VAR13')
            if d == 'F': check('VAR14')

        self.s.at_xpath("//input[@name='SUBMIT2']").click()
        if self.s.at_xpath("//td[contains(@class, 'LIST.VAR')]", timeout=8) is None:
            # We may have an error. Find the error message.
            errdiv = self.s.at_xpath("//div[@class='errorText']")
            if errdiv is None:
                raise BadPageErr('Invalid search results page')
            else:
                raise BadQueryErr('TigerPAWS says: {}'.format(errdiv.text()))
        else:
            return parse.parse_search_table(self.s.body())


class BadQueryErr(Exception):
    """This is returned from functions when an incorrect parameter is passed,
    like a term that doesn't exist."""
    pass

class BadPageErr(Exception):
    """This exception indicates that a webpage element that the program
    expected to be present could not be found."""
    pass

class LoginFailedErr(Exception):
    pass

