"""
This module includes several data structures used by PawPy.
"""

class MeetInfo(object):
    """
    This class stores information about when and where a class meets,
    specifically the information contained in the "Meeting Information" column
    on TigerPAWS's class tables.

    Note: Some classes in TigerPAWS show 'To Be Announced' in the meet info
    field. This doesn't match the regex normally used to parse meet info. When
    this happens, this class's attributes will be set to 'N/A' (days will be an
    empty list).

    Attributes:
        start_date
        end_date
        days         list of days the class meets as M/T/W/R/F strings
        start_time
        end_time
        room
    """
    def __init__(self, start_date, end_date, days, start_time, end_time, room):
        self.start_date = start_date
        self.end_date = end_date
        self.days = days
        self.start_time = start_time
        self.end_time = start_time
        self.room = room

    @staticmethod
    def not_applicable():
        """
        Creates a 'N/A' meet info object as described in the class's docstring.
        """
        return MeetInfo('N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A')

    def days_str(self):
        """
        Returns the days this class meets on as a short 'MTWRF' string.
        """
        return "".join(self.days)


class PawClass(object):
    """
    Stores basic information about a class.

    Attributes:
        title
        cid      the course's subject code and four digit ID (e.g., CSCI-1320)
        shortid  the five digit "synonym"
        meet     meeting information (see `MeetInfo`)
    """
    def __init__(self, cid, shortid, title, meet_info):
        self.title = title
        self.cid = cid
        self.shortid = shortid
        self.meet = meet_info

    def __str__(self):
        return "{} ({}) {}".format(self.cid, self.shortid, self.title)

    def display(self):
        """
        Prints a long string containing all the class's attributes. (As opposed
        to '__str__', which only prints the ID and title.)
        """
        return "{cid} {shortid} \"{title}\": {days} {start_time}-{end_time} in {room}".format(
            cid=self.cid, shortid=self.shortid, title=self.title,
            days=self.meet.days_str(),
            start_time=self.meet.start_time, end_time=self.meet.end_time,
            room=self.meet.room,
        )

class SearchClass(PawClass):
    """
    This stores information about classes returned from searching for sections.
    It contains some additional information.

    Additional Attributes:
        status    whether the class is waitlisted, open, or closed
        prof      professor
        comcurric common curriculum credits as a string
        pathways  pathways credits as a string
    """
    def __init__(self, status, prof, comcurric, pathways, **kwargs):
        super().__init__(**kwargs)
        self.status = status
        self.prof = prof
        self.comcurric = comcurric
        self.pathways = pathways

    def display(self):
        return "{cid} {shortid} \"{title}\" ({status}): {daystr} {start_time}-{end_time} {prof} in {room} ({comcurric} {pathways})".format(
            daystr=self.meet.days_str(),
            cid=self.cid, shortid=self.shortid, title=self.title,
            status=self.status, prof=self.prof,
            comcurric=self.comcurric, pathways=self.pathways,
            **self.meet.__dict__
        )
