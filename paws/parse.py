import re
from bs4 import BeautifulSoup

from paws.types import PawClass, SearchClass, MeetInfo

# This module is where we hide all the really long parsing stuff.

def parse_term_opts(body):
    """
    Parses term options and returns them in (ID, Name) tuples.
    """
    soup = BeautifulSoup(body, 'html.parser')
    dropdown = soup.find_all("select", attrs={'name': 'VAR4'})[0]
    terms = []
    for opt in dropdown.find_all("option"):
        if opt.string == None: continue
        terms.append((opt['value'], opt.string))
    return terms

def parse_pref_sects(body):
    soup = BeautifulSoup(body, 'html.parser')
    table = soup.find_all("table", attrs={'summary': 'Preferred'})[0]
    classes = []
    for row in table.find_all("tr"):
        names = row.select(".SEC_SHORT_TITLE")
        # Skip rows with no name cell or an empty name cell.
        if len(names) <= 0: continue
        # If we hit the "you are on the following waitlists" line, stop.
        if names[0].div.a == None: continue
        name_title = names[0].div.a.string
        meet_info = row.select(".SEC_MEETING_INFO")[0].div.p.string
        if name_title is None: continue # This is a workaround for things we can't parse.
        if meet_info is None: continue
        creds = row.select(".LIST_VAR3")[0].div.input.string
        cls = parse_class_info(name_title, meet_info, creds)
        if cls: classes.append(cls)
    return classes

def parse_class_table(body):
    soup = BeautifulSoup(body, 'html.parser')
    table = soup.find_all("table", attrs={'summary': 'Schedule'})[0]
    classes = []
    for row in table.find_all("tr"):
        names = row.select(".LIST_VAR6")
        # Skip rows with no name cell or an empty name cell.
        if len(names) <= 0: continue
        # If we hit the "you are on the following waitlists" line, stop.
        if "waitlist" in names[0].div.text: break
        if names[0].div.a == None: continue
        name_title = names[0].div.a.string
        meet_info = row.select(".LIST_VAR12")[0].div.p.string
        if name_title is None: continue # This is a workaround for things we can't parse.
        creds = row.select(".LIST_VAR8")[0].div.p.string
        cls = parse_class_info(name_title, meet_info, creds)
        if cls: classes.append(cls)
    return classes

def parse_search_table(body):
    soup = BeautifulSoup(body, 'html.parser')
    table = soup.find_all("table", attrs={'class': 'noSpacing'})[0]
    sects = []
    for row in table.find_all("tr")[1:]:
        cells = []
        for c in row.find_all("td"):
            cells.append(c.string)
        # There's a little mini-table in the header. I don't know why it's
        # there, but its tr elements get included in the call to
        # table.find_all("tr"). This line skips those entries.
        if len(cells) <= 2: continue
        # There are no identifying attributes on the table cells, so we have to
        # do it this way.
        sect = dict(
            status=cells[2],
            prof=cells[6],
            comcurric=cells[8],
            pathways=cells[9],
        )
        # Sometimes there's a <p> tag inside the <a> tag in this column. For
        # now we'll just give up on parsing these entries.
        if cells[3] is None: continue
        name_title = parse_name_title(cells[3])
        meet_info = parse_meet_info(cells[5])
        if name_title is None: continue

        sect.update(name_title)
        sects.append(SearchClass(
            meet_info=meet_info,
            **sect
        ))
    return sects

name_title_regex = r'([A-Z]+-[\d]+-\d+) \((\d+)\) (.+)'
def parse_name_title(name_title):
    nt = re.search(name_title_regex, name_title)
    if nt is None: return None
    return dict(
        cid = nt.group(1),
        shortid = nt.group(2),
        title = nt.group(3),
    )

meet_info_regex = r'(\d\d/\d\d/\d\d\d\d)-(\d\d/\d\d/\d\d\d\d) \S+ (\D+) (\d\d:\d\d[AP]M) - (\d\d:\d\d[AP]M), (.+)'
def parse_meet_info(meet_info):
    """
    Takes a string from TigerPAWS's "Meeting Info" column in a course list and
    returns a dict with fields for `start_date`, `end_date`, `days`,
    `start_time`, `end_time`, and `room`.
    """
    info = re.search(meet_info_regex, meet_info)
    if info is None:
        return MeetInfo.not_applicable()
    return MeetInfo(
        start_date = info.group(1),
        end_date = info.group(2),
        days = map(shorten_day, info.group(3).split(", ")),
        start_time = info.group(4),
        end_time = info.group(5),
        room = info.group(6),
    )


def parse_class_info(name_title, meet_info, creds):
    """Parses class information from columns in the class schedule page."""
    nt = parse_name_title(name_title)
    mi = parse_meet_info(meet_info)

    if nt is None:
        print("Warning: failed parsing name/title")
        return None
    if mi is None:
        print("Warning: failed parsing meeting information for {}".format(nt['cid']))
        return None

    return PawClass(nt['cid'], nt['shortid'], nt['title'], mi)

def shorten_day(day):
    if day == 'Monday': return 'M'
    if day == 'Tuesday': return 'T'
    if day == 'Wednesday': return 'W'
    if day == 'Thursday': return 'R'
    if day == 'Friday': return 'F'
